package sn.adama.demo.entites;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Compte implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double solde_veille;
    private String numero_compte;
    private Double balance;
    private String type_compte;
    private final Date date_ouverture = new Date();
    private Date blocage_compte;
    private Double solde_plafond;
    private Long id_user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Utilisateur utilisateur;

//    @OneToMany(mappedBy = "transactionTo")
//    @JsonBackReference
//    private List<Transaction> transactionTo;
//    @OneToMany(mappedBy = "transactionFrom")
//    @JsonBackReference
//    private List<Transaction> transactionsFrom;
}
