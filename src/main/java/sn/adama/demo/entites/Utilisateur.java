package sn.adama.demo.entites;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 50)
    private String prenom;
    @Column(length = 20)
    private String tel;
    @Column(length = 200)
    private String adresse;
    @Column(length = 200)
    private String email;
    @Column(length = 200)
    private String nom;
    @OneToMany(mappedBy = "utilisateur")
    @JsonBackReference
    private List<Compte> comptes;

}
