package sn.adama.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.adama.demo.entites.Utilisateur;
import sn.adama.demo.repository.UtilisateurRepository;
import sn.adama.demo.services.UtilisateurService;

@RestController
@RequestMapping("/api")
public class UtilisateurContoller {
    @Autowired
    private UtilisateurRepository userRepository;
    @Autowired
    private UtilisateurService utilisateurService;
    @PostMapping("/users/add")
    public ResponseEntity<?> addUser(@RequestBody Utilisateur user) {
        userRepository.save(user);
        System.out.println(user);
        return ResponseEntity.ok(user);
    }


    @GetMapping("/users/liste")
    public ResponseEntity<?> listeUsers() {
        return ResponseEntity.ok(userRepository
                .findAll());
    }

    @DeleteMapping("/users/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        try {
            userRepository.deleteById(id);
            return ResponseEntity.ok("utilisateur supprimé");
        } catch (Exception e) {
            return ResponseEntity.ok(new Error("Suppression echouée"));
        }

    }

    @PutMapping("/users/update")
    public @ResponseBody
    Utilisateur update(@RequestBody Utilisateur user) {
        return userRepository.save(user);
    }

    @RequestMapping("/getUserByNom/{nom}")
    public Utilisateur getNom(@PathVariable String nom) {
        return utilisateurService.findByNom(nom);
    }
}
