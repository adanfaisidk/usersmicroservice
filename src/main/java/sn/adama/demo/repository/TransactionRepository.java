package sn.adama.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.adama.demo.entites.Transaction;
import sn.adama.demo.entites.Utilisateur;
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
